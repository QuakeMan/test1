#ifndef BASEELEMENT_H
#define BASEELEMENT_H

#include <list>
#include <string>
#include <sstream>

using namespace std;
enum elementType {start2d, line2d, start3d, line3d, arc2d};

class BaseElement
{
public:
    BaseElement();
    virtual ~BaseElement();
    virtual string toString() = 0;
    elementType getType() const;
    virtual double getX() const;
    virtual double getY() const;

protected:
    elementType type;
    double m_X;
    double m_Y;
};

#endif // BASEELEMENT_H
