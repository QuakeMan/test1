#ifndef START2D_H
#define START2D_H
#include "include/baseelement.h"

class Start2d : public BaseElement
{
public:
    Start2d(double x, double y);
    virtual ~Start2d();
    std::string toString();
};

#endif // START2D_H
