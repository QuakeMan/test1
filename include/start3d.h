#ifndef START3D_H
#define START3D_H
#include <./include/baseelement.h>

class Start3d : public BaseElement
{
public:
    Start3d(double x, double y, double z)
    {
        m_X = x;
        m_Y = y;
        m_Z = z;
        type = start3d;
    }
    ~Start3d();
    std::string toString();
};
#endif // START3D_H
