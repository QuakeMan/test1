#ifndef DTOP_H
#define DTOP_H

#include <cmath>

double getangle(double x, double y);
double getradius(double x, double y);
void getline(double x, double y, double angle, double &k, double &b);

#endif // DTOP_H