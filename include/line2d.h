#ifndef LINE_H
#define LINE_H
#include "include/baseelement.h"


class Line2d : public BaseElement
{
public:
    Line2d(double x, double y);
    virtual ~Line2d();
    std::string toString();
};

#endif // LINE_H
