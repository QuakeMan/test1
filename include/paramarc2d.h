#ifndef PARAMARC_H
#define PARAMARC_H

#include "include/arc2d.h"
enum arcWay;


struct BaseParamArc2d {
    double centerX;
    double centerY;
    double angle;
    double beginX;
    double beginY;
    double endX;
    double endY;

    BaseParamArc2d(double ox, double oy, double bx, double by, double ex, double ey, double an);
    virtual bool check();
    virtual void printme();
};

struct FirstWayParamArc2d : public BaseParamArc2d {
    double deltaX;
    double deltaY;
    arcWay way;
    virtual void printme();
    FirstWayParamArc2d(double ox, double oy, double bx, double by, double ex, double ey, double dx, double dy, arcWay w, double an);
};

struct SecondWayParamArc2d : public BaseParamArc2d {
    double radius;
    arcWay way;
    virtual void printme();
    SecondWayParamArc2d(double ox, double oy, double bx, double by, double ex, double ey, double rad, arcWay w, double an);
};

struct ThirdWayParamArc2d : public BaseParamArc2d {
    double pointX;
    double pointY;
    virtual void printme();
    ThirdWayParamArc2d (double ox, double oy, double bx, double by, double ex, double ey, double x, double y, double an);
};

#endif // PARAMARC_H
