#ifndef ARC_H
#define ARC_H
#include "include/baseelement.h"
#include <cmath>
#include <ctime>
#include "include/dtop.h"

enum arcWay { CW, CCW };
struct FirstWayParamArc2d;
struct SecondWayParamArc2d;
struct ThirdWayParamArc2d;

#include "include/paramarc2d.h"


class Arc2d : public BaseElement
{
    /*угол согласно полярной системе координат, точка ноль совпадает с центром дуги
    знак угла определяет направление дуги*/
    double angle;
    //стартовая точка
    double b_X, b_Y;
public:
    Arc2d(double dx, double dy, double ex, double ey, arcWay way, BaseElement *prevEl);
    Arc2d(double radius, double ex, double ey, arcWay way, BaseElement * prevEl);
    Arc2d(double ax, double ay, double ex, double ey, BaseElement * prevEl);

    virtual ~Arc2d();
    std::string toString();
    std::string getStringFirstWay();
    std::string getStringSecondWay();
    std::string getStringThirdWay();
    FirstWayParamArc2d getFirstWay();
    SecondWayParamArc2d getSecondWay();
    ThirdWayParamArc2d getThirdWay();
    double getX() const;
    double getY() const;
};

#endif // ARC_H
