#-------------------------------------------------
QT       += core
QT       -= gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Test
CONFIG += console
CONFIG -= app_bundle
TEMPLATE = app
DESTDIR= "$$PWD"

SOURCES += src/main.cpp \
    src/baseelement.cpp \
    src/start2d.cpp \
    src/line2d.cpp \
    src/arc2d.cpp \
	src/dtop.cpp \
    src/paramarc2d.cpp
	
HEADERS  += include/baseelement.h \
    include/start2d.h \
    include/line2d.h \
    include/arc2d.h \
	include/dtop.h \
    include/paramarc2d.h

CONFIG += c++11

DISTFILES += \
    README.md
