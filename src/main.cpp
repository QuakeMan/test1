#include <iostream>
#include <list>
#include "include/line2d.h"
#include "include/start2d.h"
#include "include/arc2d.h"

//#include "include/dtop.h"

using namespace std;

int main(int argc, char *argv[])
{
    //getangle test
    cout << getangle(10.0, 10) << endl;
    cout << getangle(0, 10) << endl;
    cout << getangle(-10.0, 10) << endl;
    cout << getangle(-10.0, 0) << endl;
    cout << getangle(-10.0, -10) << endl;
    cout << getangle(0, -10) << endl;
    cout << getangle(10.0, -10) << endl;
    cout << getangle(0, 0) << endl;

    cout << endl;

    list<BaseElement*> contour;

    contour.push_back(new Start2d(0, 0));
    contour.push_back(new Line2d(0, 100));
    contour.push_back(new Line2d(100, 100));
    contour.push_back(new Line2d(100, 0));
    contour.push_back(new Line2d(0, 0));

    for (BaseElement* &element : contour)
    {
        cout << element->toString() << endl;
        delete element;
    }

    cout << endl;
    auto temptest = new Arc2d(5.0, 5.0, 10.0, 10.0, CCW, new Start2d(0, 0));
    //new Arc2d(5.0, 5.0, 9, 10.0, CCW, new Start2d(0, 0));
    cout << temptest->getStringFirstWay() << endl;
    cout << temptest->getStringSecondWay() << endl;
    cout << endl;
    cout << (new Arc2d(-5.0, -5.0, 0, 0, CCW, temptest))->getStringFirstWay() << endl;
    cout << endl << endl;

    auto temptest1 = new Arc2d(10.0, -10.0, 10.0, 10.0, CCW, new Start2d(-10.0, 10));
    //new Arc2d(5.0, 5.0, 9, 10.0, CCW, new Start2d(0, 0));
    cout << temptest1->getStringFirstWay() << endl;
    cout << temptest1->getStringSecondWay() << endl;
    cout << endl;
    auto temptest2 = new Arc2d(-10.0, -10.0, -10.0, 10.0, CW, temptest1);
    cout << temptest2->getStringFirstWay() << endl;
    cout << temptest2->getStringSecondWay() << endl;

    cout << endl;
    cout << endl;
    //second way
    auto start = new Start2d(7, 7);
    auto temptest3 = new Arc2d(4.0, 5, 5, CCW, start);
    cout << temptest3->getStringFirstWay() << endl;
    cout << temptest3->getStringSecondWay() << endl;
    cout << temptest3->getStringThirdWay() << endl << endl;

    auto temptest4 = new Arc2d(1.645751, -3.645751, 5, 5, CCW, start);
    cout << temptest4->getStringFirstWay() << endl;
    cout << temptest4->getStringSecondWay() << endl;
    cout << temptest4->getStringThirdWay() << endl << endl;

    auto temptest5 = new Arc2d(-4.0, -1, 7, CCW, start);
    cout << temptest5->toString() << endl;
    cout << temptest5->getStringFirstWay() << endl;
    cout << temptest5->getStringSecondWay() << endl;
    cout << temptest5->getStringThirdWay() << endl << endl;

    auto temptest6 = new Arc2d(4, 3, 5, 5, start);
    cout << temptest6->toString() << endl;
    cout << temptest6->getStringFirstWay() << endl;
    cout << temptest6->getStringSecondWay() << endl;
    cout << temptest6->getStringThirdWay() << endl << endl;

    auto temptest7 = new Arc2d(5, 5, 4, 3, start);
    cout << temptest7->toString() << endl;
    cout << temptest7->getStringFirstWay() << endl;
    cout << temptest7->getStringSecondWay() << endl;
    cout << temptest7->getStringThirdWay() << endl << endl;

    Start2d startarr[]{ Start2d(10, 0), Start2d(0, -10), Start2d(-10, 0), Start2d(0, 10) };


    Arc2d temp(-10, 0, 0, 10, CCW, &startarr[0]);
    temp.getFirstWay().printme();

    temp = Arc2d (0, -10, 0, -10, CCW, &startarr[0]);
    temp.getFirstWay().printme();

    temp = Arc2d(-10, 0, 0, -10, CW, &startarr[1]);
    temp.getFirstWay().printme();

    temp = Arc2d(-10, 0, -10, 00, CW, &startarr[1]);
    temp.getFirstWay().printme();

    temp = Arc2d(0, 10, 0, 10, CCW, &startarr[1]);
    temp.getFirstWay().printme();
    temp.getSecondWay().printme();
    temp.getThirdWay().printme();

    temp = Arc2d(10, 10, 0, 10, CCW, &startarr[1]);
    temp.getFirstWay().printme();
    temp.getSecondWay().printme();
    temp.getThirdWay().printme();

    temp = Arc2d(10, 10, 0, 10, CCW, &startarr[1]);
    temp.getFirstWay().printme();
    temp.getSecondWay().printme();
    temp.getThirdWay().printme();

    temp = Arc2d(-10, 10, 0, CCW, &startarr[1]);
    temp.getFirstWay().printme();
    temp.getSecondWay().printme();
    temp.getThirdWay().printme();



    return 0;
}

