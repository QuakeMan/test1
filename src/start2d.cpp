#include "include/start2d.h"

Start2d::Start2d(double x, double y)
{
    m_X = x;
    m_Y = y;
    type =start2d ;
}

Start2d::~Start2d()
{

}

std::string Start2d::toString()
{
    return "Start2d: x = " + to_string(m_X) + ", y = " + to_string(m_Y);
}
