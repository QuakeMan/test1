#include "include/arc2d.h"
#include <iostream>
#include <iomanip>

Arc2d::Arc2d(double dx, double dy, double ex, double ey, arcWay way, BaseElement * prevEl)
{
    b_X = prevEl->getX();
    b_Y = prevEl->getY();
    m_X = b_X + dx;
    m_Y = b_Y + dy;
    //to del
    //std::cout << "e_X = " << ex << std::endl;
    //std::cout << "e_Y = " << ey << std::endl;
    if (abs(getradius(dx, dy) - getradius(m_X - ex, m_Y - ey)) > 0.00000001) {
        std::cout << "some error with data " << getradius(dx, dy) << " " << getradius(m_X - ex, m_Y - ey) << endl;
        exit(EXIT_FAILURE);
    }
    auto bangle = getangle(b_X - m_X, b_Y - m_Y);
    auto eangle = getangle(ex - m_X, ey - m_Y);
    angle = eangle - bangle;
    if (bangle > eangle) angle = way ? 2.0 * acos(-1.0) + angle : angle;
    else if (bangle < eangle) angle = way ? angle : angle -2.0 * acos(-1.0);
    //to del
    //std::cout << "begangle = " << getangle(b_X - m_X, b_Y - m_Y) << std::endl;
    //std::cout << "endangle = " << getangle(ex - m_X, ey - m_Y) << std::endl;
    //std::cout << "angle = " << angle << std::endl;
}

Arc2d::Arc2d(double radius, double ex, double ey, arcWay way, BaseElement * prevEl) {
    b_X = prevEl->getX();
    b_Y = prevEl->getY();
    if (getradius(ex - b_X, ey - b_Y) - 2.0 * abs(radius) > 0.00000001) {
        std::cout << "some error with data " << radius << " " << getradius(ex - b_X, ey - b_Y) << endl;
        exit(EXIT_FAILURE);
    }
    if (abs(abs(radius * 2) - getradius(ex - b_X, ey - b_Y)) < 0.00000001) {
        m_X = (ex + b_X) /2.0;
        m_Y = (ey + b_Y) /2.0;
    } else {
        //решаю через уравнения прямых и пифагора
        auto tempangle = atan2(b_Y - ey, b_X - ex);//угол хорды к оси X
        //центр хорды
        auto abx = (ex + b_X) /2.0;
        auto aby = (ey + b_Y) /2.0;
        tempangle = tempangle + asin(1);//угол прямой проходящей через центры к оси X
        //модуль отрезка между центром хорды и центрами дуг
        auto d_ab_o = sqrt(pow(radius, 2.0) - pow((ex - b_X) / 2, 2.0) - pow((ey - b_Y) / 2, 2.0));
        if (radius < 0) {
            m_Y = aby + d_ab_o * sin(tempangle);
            m_X = abx + d_ab_o * cos(tempangle);
        } else {
            m_Y = aby - d_ab_o * sin(tempangle);
            m_X = abx - d_ab_o * cos(tempangle);
        }
    }
    auto bangle = getangle(b_X - m_X, b_Y - m_Y);
    auto eangle = getangle(ex - m_X, ey - m_Y);
    angle = eangle - bangle;
    if (bangle > eangle) angle = way ? 2.0 * acos(-1.0) + angle : angle;
    else if (bangle < eangle) angle = way ? angle : angle - 2.0 * acos(-1.0);

}

Arc2d::Arc2d(double ax, double ay, double ex, double ey, BaseElement * prevEl) {
    b_X = prevEl->getX();
    b_Y = prevEl->getY();
    auto tempangle = atan2(b_Y - ey, b_X - ex);//угол хорды к оси X
    tempangle = tempangle + asin(1);//угол прямой перпендикулярной хорде
    //центр хорды
    auto abx = (ex + b_X) /2.0;
    auto aby = (ey + b_Y) /2.0;
    double k1, b1;
    //std::cout << "abx = " << abx << " aby = " << aby << endl;
    getline(abx, aby, tempangle, k1, b1);
    //std::cout << "angle1 = " << tempangle * 180 / acos(-1);
    tempangle = atan2(ay - ey, ax - ex);
    tempangle = tempangle + asin(1);
    abx = (ex + ax) /2.0;
    aby = (ey + ay) /2.0;
    double k2, b2;
    //std::cout << " angle2 = " << tempangle * 180 / acos(-1) << endl;
    //std::cout << "abx = " << abx << " aby = " << aby << endl;
    getline(abx, aby, tempangle, k2, b2);
    //std::cout << "k1 = " << k1 << " k2 = " << k2 << endl;
    //std::cout << "b1 = " << b1 << " b2 = " << b2 << endl;
    if (abs(k1 - k2) < 0.00000001) {
        std::cout << "some error with data all point on one line" << endl;
        exit(EXIT_FAILURE);
    }
    m_X = (b2 - b1) / (k1 - k2);
    m_Y = k1 * m_X + b1;
    auto bangle = getangle(b_X - m_X, b_Y - m_Y);
    auto aangle = getangle(ax - m_X, ay - m_Y);
    auto eangle = getangle(ex - m_X, ey - m_Y);
    //std::cout << "bangle = " << bangle << " aangle = " << aangle << " eangle = " << eangle << std::endl;
    angle = aangle > eangle ?  eangle - acos(-1) *2.0 - bangle : eangle - bangle;
    //std::cout << "angle = " << angle << std::endl;
    //if (bangle > eangle) angle = way ?2.0* acos(-1.0) + angle : angle;
    //else if (bangle < eangle) angle = way ? angle : angle -2.0* acos(-1.0);
}

Arc2d::~Arc2d() {}

std::string Arc2d::toString() {
    return "Arc2d: x = " + to_string(m_X) + ", y = " + to_string(m_Y);
}

std::string Arc2d::getStringFirstWay() {
    return "Arc2d, first way: d_x = " + to_string(m_X - b_X) + ", d_y = " + to_string(m_Y - b_Y) + ", e_x = " +
        to_string(getX()) + ", e_y = " + to_string(getY()) + (angle > 0 ? ", counterclockwise" : ", clockwise");
}

std::string Arc2d::getStringSecondWay() {
    auto radius = getradius(b_X - m_X, b_Y - m_Y);
    if (angle > acos(-1) || (angle > -acos(-1) && angle < 0)) radius = -radius;
    return "Arc2d, second way: radius = " + to_string(radius) + ", e_x = " + to_string(getX()) +
        ", e_y = " + to_string(getY()) + (angle > 0 ? ", counterclockwise" : ", clockwise");
}

std::string Arc2d::getStringThirdWay() {
    srand(time(NULL));
    auto radius = getradius(b_X - m_X, b_Y - m_Y);
    auto randangle = getangle(b_X - m_X, b_Y - m_Y) + angle / (double(rand() % 5) + 0.01);
    return "Arc2d, third way: ax = " + to_string(m_X + radius * cos(randangle)) +
        ", ay = " + to_string(m_Y + radius * sin(randangle)) +
        ", e_x = " + to_string(getX()) + ", e_y = " + to_string(getY());
}

FirstWayParamArc2d Arc2d::getFirstWay()
{
    FirstWayParamArc2d returnparam(m_X, m_Y, b_X, b_Y, getX(), getY(), m_X - b_X, m_Y - b_Y, angle > 0 ? CCW : CW, angle);
    return returnparam;
}

SecondWayParamArc2d Arc2d::getSecondWay()
{
    SecondWayParamArc2d returnparam(m_X, m_Y, b_X, b_Y, getX(), getY(), 
        getradius(b_X - m_X, b_Y - m_Y), angle > 0 ? CCW : CW, angle);
    return returnparam;
}

ThirdWayParamArc2d Arc2d::getThirdWay()
{
    auto radius = getradius(b_X - m_X, b_Y - m_Y);
    auto randangle = getangle(b_X - m_X, b_Y - m_Y) + angle / (double(rand() % 5) + 0.01);
    ThirdWayParamArc2d returnparam(m_X, m_Y, b_X, b_Y, getX(), getY(), 
        m_X + radius * cos(randangle), m_Y + radius * sin(randangle), angle);
    return returnparam;
}

double Arc2d::getX() const {
    auto radius = getradius(b_X - m_X, b_Y - m_Y);
    auto deltaangle = getangle(b_X - m_X, b_Y - m_Y);
    return m_X + radius * cos(angle + deltaangle);
}

double Arc2d::getY() const {
    auto radius = getradius(b_X - m_X, b_Y - m_Y);
    auto deltaangle = getangle(b_X - m_X, b_Y - m_Y);
    return m_Y + radius * sin(angle + deltaangle);
}
