#include "include/dtop.h"

double getangle(double x, double y) {
    if (!y) return x < 0 ? acos(-1.0) : 0;
    if (y >= 0) return acos(x / getradius(x, y));
    //return acos(x / getradius(x, y));
    //return  -acos(x / getradius(x, y));
    return  2 * acos(-1.0) - acos(x / getradius(x, y));
}

double getradius(double x, double y) {
    return sqrt(pow(x, 2.0) + pow(y, 2.0));
}

void getline(double x, double y, double angle, double &k, double &b) {
    k = tan(angle);
    b = y - k * x;
}

