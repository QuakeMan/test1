#include <./include/line3d.h>


Line3d::~Line3d()
{

}

std::string Line3d::toString()
{
    return "Line3d: x = " + std::to_string(m_X) + ", y = " +
            std::to_string(m_Y) + ", z = " + std::to_string(m_Z);
}
