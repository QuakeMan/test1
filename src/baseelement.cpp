#include <./include/baseelement.h>

BaseElement::BaseElement():
    m_X(0),m_Y(0)
{

}

BaseElement::~BaseElement()
{

}

elementType BaseElement::getType() const
{
    return type;
}

double BaseElement::getX() const
{
    return m_X;
}

double BaseElement::getY() const
{
    return m_Y;
}
