#include "include/paramarc2d.h"
#include "include/dtop.h"
#include <iostream>
#include <cmath>
#include <iomanip>

BaseParamArc2d::BaseParamArc2d(double ox, double oy, double bx, double by, double ex, double ey, double an)
    :centerX(ox),centerY(oy), beginX(bx), beginY(by), endX(ex), endY(ey), angle(an) {}

bool BaseParamArc2d::check() {
    return abs(getradius(beginX - centerX, beginY - centerY) -
        getradius(endX - centerX, endY - centerY)) < 0.00000001;
}

void BaseParamArc2d::printme(){
    std::cout << std::setprecision(4) <<
        "Arc2d, OX = " << std::setw(10) << centerX << "   OY = " << std::setw(10) << centerY << std::endl <<
        "       BX = " << std::setw(10) << beginX << "   BY = " << std::setw(10) << beginY << std::endl <<
        "       EX = " << std::setw(10) << endX << "   EY = " << std::setw(10) << endY << std::endl <<
        "    angle = " << std::setw(10) << angle * 180 / acos(-1) << std::endl;
}

void FirstWayParamArc2d::printme(){
    BaseParamArc2d::printme();
    std::cout << std::setprecision(4) <<
        "       DX = " << std::setw(10) << deltaX << "   DY = " << std::setw(10) << deltaY << std::endl <<
        (angle > 0 ? "counterclockwise" : "clockwise") << std::endl << std::endl;
}

FirstWayParamArc2d::FirstWayParamArc2d
    (double ox, double oy, double bx, double by, double ex, double ey, double dx, double dy, arcWay w, double an)
    :BaseParamArc2d(ox, oy, bx, by, ex, ey, an), deltaX(dx), deltaY(dy), way(w) {}

void SecondWayParamArc2d::printme(){
    BaseParamArc2d::printme();
    std::cout << std::setprecision(4) <<
        "   radius = " << std::setw(10) << radius << std::endl <<
        (way ? "counterclockwise" : "clockwise") << std::endl << std::endl;
}

SecondWayParamArc2d::SecondWayParamArc2d
    (double ox, double oy, double bx, double by, double ex, double ey, double rad, arcWay w, double an)
    : BaseParamArc2d(ox, oy, bx, by, ex, ey, an), radius(rad), way(w) {}

void ThirdWayParamArc2d::printme(){
    BaseParamArc2d::printme();
    std::cout << std::setprecision(4) <<
        "        X = " << std::setw(10) << pointX << "    Y = " << std::setw(10) << pointY << std::endl <<
        (angle > 0 ? "counterclockwise" : "clockwise") << std::endl << std::endl;
}

ThirdWayParamArc2d::ThirdWayParamArc2d
    (double ox, double oy, double bx, double by, double ex, double ey, double x, double y, double an)
    : BaseParamArc2d(ox, oy, bx, by, ex, ey, an), pointX(x), pointY(y) {}
